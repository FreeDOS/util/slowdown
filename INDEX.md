# SLOWDOWN

Slow down a fast computer. Speed is completely configurable from command line, or by using Hot-Keys. Has several different "methods" or "modes" to slow the computer with.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SLOWDOWN.LSM

<table>
<tr><td>title</td><td>SLOWDOWN</td></tr>
<tr><td>version</td><td>3.10a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-05-14</td></tr>
<tr><td>description</td><td>Slow down a fast computer</td></tr>
<tr><td>keywords</td><td>DOS</td></tr>
<tr><td>author</td><td>Bret Johnson &lt;bret@bretjohnson.us&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Bret Johnson &lt;bret@bretjohnson.us&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://bretjohnson.us/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://bretjohnson.us/</td></tr>
<tr><td>original&nbsp;site</td><td>http://bretjohnson.us/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Freeware, see documentation</td></tr>
<tr><td>summary</td><td>Slow down a fast computer. Speed is completely configurable from command line, or by using Hot-Keys. Has several different "methods" or "modes" to slow the computer with.</td></tr>
</table>
