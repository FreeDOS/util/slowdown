  JMP  Main ;Skip over Data to the Code

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;EQUATES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;General Equates, needed with almost every program
  No  EQU  0 ;Misc Yes/No Tests
  Yes EQU -1 ;Misc Yes/No Tests
  LF  EQU 10 ;Line Feed
  CR  EQU 13 ;Carriage Return
  EOF EQU 26 ;End-of-File

;Cache Disable Bit Mask of CPU's CR0 (Control Register 0)
  CacheDisable EQU 4000_0000h ;Cache Disable Bit of CR0 Register
  NoWriteThru  EQU 2000_0000h ;Cache Write-Through Disable Bit of CR0 Register

;ErrorLevels returned by Program
  ErLvlEnabled  EQU 0 ;Errorlevel if Cache is Enabled
  ErLvlDisabled EQU 1 ;Errorlevel if Cache is Disabled
  ErLvlError    EQU 2 ;Errorlevel if CPU Error

;Possible things that the user may want us to do
  DoStatus  EQU 0 ;Show the Status
  DoEnable  EQU 1 ;Enable Cache
  DoDisable EQU 2 ;Disable Cache
  DoToggle  EQU 3 ;Toggle Cache Status
  DoHelp    EQU 4 ;User needs help


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;MACROS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  JUMP  MACRO  JMP SHORT #1 #EM  ;Short Jump, to reduce program size


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;BYTE/WORD/DWORD Data (Strings and such)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Help message must be the first data at the top of the file!!
;If the user tries to TYPE the COM file, they'll get this Help Message
;  instead of a bunch of beeps and happy faces like most programs do.

  HelpMsg   DB  CR
            DB  'CPUCACHE 1.00, (C) 2002 Bret Johnson.',CR,LF
            DB  LF
            DB  "CPUCACHE will ENABLE, DISABLE, TOGGLE, or simply show you the status",CR,LF
            DB  "  of your CPU's Cache.",CR,LF
            DB  LF
            DB  "If the Cache is ENABLED, your computer will run MANY TIMES faster than",CR,LF
            DB  "  it will if the Cache is DISABLED.",CR,LF
            DB  "By default, most computers boot up with the Cache ENABLED.",CR,LF
            DB  "DISABLING the Cache will slow the computer way down, and may allow you to run",CR,LF
            DB  "  older programs that can sometimes crash a computer that is too fast.",CR,LF
            DB  LF
            DB  "To use CPUCACHE, type one of the following from DOS:",CR,LF
            DB  LF
            DB  "  CPUCACHE On      ENABLEs the CPU Cache",CR,LF
            DB  "  CPUCACHE Off     DISABLEs the CPU Cache",CR,LF
            DB  "  CPUCACHE Toggle  TOGGLEs the state of the CPU Cache",CR,LF
            DB  "  CPUCACHE         Simply shows the current state of the CPU Cache",CR,LF
            DB  LF
            DB  "CPUCACHE returns the following ErrorLevels:",CR,LF
            DB  "  ErrorLevel 0     If the Cache is ENABLED",CR,LF
            DB  "  ErrorLevel 1     If the Cache is DISABLED",CR,LF
            DB  "  ErrorLevel 2     If the CPU does not have a Cache, or it cannot be changed",CR,LF
            DB  0
            DB  EOF

  NoMsg       DB  0
  ChangedMsg  DB  "Attempted to change the status of the CPU Cache.",CR,LF,0
  CacheMsg    DB  "The CPU Cache is currently ",0
  Enabledmsg  DB  "Enabled.",0
  Disabledmsg DB  "Disabled.",0
  CPUErrMsg   DB  "This computer is not compatible with CPUCACHE.",CR,LF
              DB  LF
              DB  "Either this CPU does not have a Cache, or your are running underneath an",CR,LF
              DB  "  Operating Environment (like Windows) that will not let DOS change the Cache.",0

  WhatToDo    DB  DoStatus  ;What the user has requested us to do


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;TABLE OF VALID COMMAND LINE SWITCHES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;If user enters an invalid switch (anything not in the Table), just do Help
  ;If they enter nothing (no switches), just show the Status

  SwitchList DB  'TOGGLE',0
             DB   0, 'T',0
                ; ^              Beginning-of-List Marker
                ;    ^   ^        ASCII Codes
                ;           ^      End-of-List Marker

             DB  'YES',0
             DB  'Y',0
             DB   0, 'E',0

             DB  'TRUE',0
             DB   0, 'E',0

             DB  'ON',0
             DB   0, 'E',0

             DB  'OFF',0
             DB   0, 'D',0

             DB  'NO',0
             DB  'N',0
             DB   0, 'D',0

             DB  'FALSE',0
             DB   0, 'D',0

             DB  'ENABLE',0
             DB   0, 'E',0

             DB  'DISABLE',0
             DB   0, 'D',0

             DB  'CHANGE',0
             DB   0, 'T',0

             DB  '+',0
             DB   0, 'E',0

             DB  '-',0
             DB   0, 'D',0

             DB  0                    ;End of List

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;TABLE OF OFFSETS FOR COMMAND LINE SWITCH SUBROUTINES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  SwitchTbl  DB  'D'
                DW OFFSET DoSwitchD      ;Disable
             DB  'E'
                DW OFFSET DoSwitchE      ;Enable
             DB  'T'
                DW OFFSET DoSwitchT      ;Toggle

             DB  0                       ;End of Table

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;PROGRAM CODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;------------------------------------------------------------------------------
;RUN OUR PROGRAM
;Inputs:
;Outputs:
;Changes: Lots of Stuff
;------------------------------------------------------------------------------
Main:
  CLD                        ;Go forward with string functions
  MOV  SI,81h                ;Parse the command line
  CALL GetSwitch             ;  for a switch
  CALL Help                  ;Do Help and Quit, if necessary
  CALL TestCacheControl      ;Test for compatibility - it not, quit
M10:                         ;Disable?
  CMP  WhatToDo,DoDisable    ;Is it Disable?
  JNE >M20                   ;If not, try the next possibility
  CALL DisableCache          ;Disable the Cache
  JMP >M50                   ;Jump to show the status
M20:                         ;Enable?
  CMP  WhatToDo,DoEnable     ;Is it Enable?
  JNE >M30                   ;If not, try the next possibility
  CALL EnableCache           ;Enable the Cache
  JMP >M50                   ;Jump to show the status
M30:                         ;Toggle?
  CMP  WhatToDo,DoToggle     ;Is it Toggle?
  JNE >M70                   ;If not, just show the status
  CALL ToggleCache           ;Toggle the Cache
;  JMP >M50                   ;Jump to show the status
M50:                         ;We changed it
  MOV  DX,OFFSET ChangedMsg  ;Write the
  CALL WriteZCon             ;  "changed It" Message
M70:                         ;Write the current Status
  CALL GetStatus             ;Get the Status

Exit:
  CALL WriteZCon             ;Write message to the screen
  MOV  AL,BL                 ;Put ErrorLevel in AL
  MOV  AH,4Ch                ;Quit with ErrorLevel in AL
  INT  21h

;------------------------------------------------------------------------------
;GET THE SWITCH CHARACTERS, AND DO WHATEVER IT IS THEY'RE ASKING
;Inputs : DS:[SI] = Command line pointer
;Outputs: WhatToDo
;Changes: SI
;------------------------------------------------------------------------------
GetSwitch:
  PUSH AX,BX,DX             ;Save used registers
  CALL IsSpaceOrEOL         ;Look for a valid character
  JC  >S90                  ;If EOL, we're done
  DEC  SI                   ;If a real character, point back at it
  MOV  BX,OFFSET SwitchList ;Point at the list of switch code words
  CALL GetCode              ;See if its a match (returns AX)
  JNC >S10                  ;If so, jump to test it
  LODSB                     ;If no match, it's a single character
  CALL Capitalize           ;Capitalize it
S10:                        ;Have our
  MOV  BX,OFFSET SwitchTbl  ;Point at the Table of Offsets
S20:                        ;Loop to here for each table entry
  MOV  DH,[BX]              ;Get the switch from our table
  OR   DH,DH                ;Is it the end of the table?
  JZ  >S80                  ;If so, the user needs help
  CMP  AL,DH                ;If a valid entry, is it a match?
  JE  >S30                  ;If so, jump to handle it
  ADD  BX,3                 ;If not, point at the next table entry
  JMP  S20                  ;And keep looking
S30:                        ;It's a match from the table
  CALL W [BX+1]             ;Do the Switch stuff
  JMP >S90                  ;And quit
S80:                        ;Invalid switch - User needs help
  MOV  WhatToDo,DoHelp      ;Mark the "Do Help" flag
S90:                        ;We're done
  POP  DX,BX,AX             ;Restore used registers
  RET

DoSwitchD:                ;Disable
  MOV  WhatToDo,DoDisable
  RET

DoSwitchE:                ;Enable
  MOV  WhatToDo,DoEnable
  RET

DoSwitchT:                ;Toggle
  MOV  WhatToDo,DoToggle
  RET

;------------------------------------------------------------------------------
;SKIP OVER SPACES TO FIND LEGITIMATE CHARACTERS, AND TEST FOR EOL
;Inputs:  DS:[SI] = Command Line Pointer
;Outputs: CF  = Clear if legitimate character
;          AL = Character
;          SI = Pointer to spot after the character
;         CF = Set if EOL
;          AL = CR
;Changes:
;------------------------------------------------------------------------------
IsSpaceOrEOL:
  LODSB             ;Get the next character
  CMP  AL,' '       ;Is it a space?
  JE   IsSpaceOrEOL ;If so, get the next character
  CMP  AL,CR        ;Is it the end of the command line?
  JE  >S80          ;If so, we're done
  CLC               ;If not, it's a legitimate character - set the OK flag
  JMP >S90          ;And quit
S80:                ;It's an End-of-line
  STC               ;Mark the End-Of-Line flag
S90:                ;We're done
  RET

;------------------------------------------------------------------------------
;CONVERT A CODE WORD FROM THE COMMAND LINE TO A WORD FROM THE LOOKUP TABLE
;Inputs:  DS:[SI] = Command Line Pointer
;         DS:[BX] = List to Test
;Outputs: AX = Word from table
;         CF = Set if no match found (error)
;            = Clear if a match was found
;Changes: SI if valid match found
;------------------------------------------------------------------------------
GetCode:
  PUSH BX,DX       ;Save used registers
  MOV  DX,AX       ;Save AX in case there's no match
C10:               ;Loop to here for each list of words
  CMP  B [BX],0    ;Are we at the end of our table?
  JE  >C80         ;If so, we couldn't find a match
  XOR  AH,AH       ;Assume it's a match
  PUSH SI          ;Save our command line pointer for a second
C20:               ;Loop to here for each character in a word
  LODSB            ;Get the next character from the command line
  CALL Capitalize  ;Capitalize it
  CMP  AL,[BX]     ;Is it a match?
  JE  >C30         ;If so, skip down
  INC  AH          ;If not, mark us as no match
C30:               ;The character is tested
  INC  BX          ;Point at the next character in our test word
  CMP  B [BX],0    ;Are we at the end of the word?
  JNE  C20         ;If not, keep going until we're done
  INC  BX          ;Point at the next word or code list
  OR   AH,AH       ;Was it a match?
  JZ  >C50         ;If so, jump to handle it
  POP  SI          ;If not, restore our command line pointer
  CMP  B [BX],0    ;Is this the end of our word list?
  JNE  C10         ;If not, just test the next word
  ADD  BX,3        ;If so, skip over the codes
  JMP  C10         ;Keep going until we're done with our list
C50:               ;We've found a match!
  POP  AX          ;Lose our original command line pointer
  DEC  BX          ;Point back at the end-of-word marker
C60:               ;Loop to here to find the end-of-list
  MOV  AX,[BX]     ;Get the next two characters in our list
  INC  BX          ;Point at the next character for next time
  OR   AX,AX       ;Is this the start of our code list?
  JNE  C60         ;If not, keep looking
  INC  BX          ;If so, point at the word we're looking for
  MOV  DX,[BX]     ;Get the word from the table
  CLC              ;Mark our OK flag
  JMP >C90         ;And we're done
C80:               ;No match found
  STC              ;Mark us as no match
C90:               ;We're done
  MOV  AX,DX       ;Set AX to the correct value
  POP  DX,BX       ;Restore used registers
  RET

;------------------------------------------------------------------------------
;WRITE THE HELP SCREEN AND QUIT, IF NECESSARY
;Inputs:  WhatToDo
;Outputs:
;Changes: Writes Help Screen and Quits, if WhatToDo = DoHelp
;------------------------------------------------------------------------------
Help:
  CMP  WhatToDo,DoHelp   ;Does the user want (or need) help?
  JNE >H90               ;If not, we're done
  MOV  DX,OFFSET HelpMsg ;If so, point at the Help Message
  XOR  BL,BL             ;ErrorLevel = 0
  JMP  Exit              ;Quit
H90:                     ;No help needed
  RET

;------------------------------------------------------------------------------
;CALCULATE AND WRITE THE CURRENT CACHE STATUS
;Inputs:
;Outputs: BL   = ErrorLevel to Quit Program with
;         [DX] = Status Message to print to CON
;Changes:
;------------------------------------------------------------------------------
GetStatus:
  PUSH EAX                   ;Save used registers
  MOV  DX,OFFSET CacheMsg    ;Write the first part of
  CALL WriteZCon             ;  the Message
  MOV  DX,OFFSET EnabledMsg  ;Assume the Cache
  MOV  BL,ErLvlEnabled       ;  is Enabled
  MOV  EAX,CR0               ;Is it
  TEST EAX,CacheDisable      ;  Enabled?
  JZ  >S90                   ;If so, we're done
  MOV  DX,OFFSET DisabledMsg ;If not, it's
  MOV  BL,ErLvlDisabled      ;  Disabled
S90:                         ;We're done
  POP  EAX                   ;Restore used registers
  RET

;------------------------------------------------------------------------------
;TEST AND SEE IF WE CAN CONTROL (ENABLE/DISABLE) THE CPU CACHE
;Inputs:
;Outputs: Quits the program if CPU is not compatible
;         Just returns if it is compatible
;Changes: EAX,EBX
;------------------------------------------------------------------------------
TestCacheControl:
  CALL Test80486           ;Is the CPU at least an 80486?
  JC  >C70                 ;If not, it's incompatible
  CLI                      ;Disable interrupts
  MOV  EBX,CR0             ;Get current Control Register 0
  CALL ToggleCache         ;Try to Toggle the status of the Cache
  MOV  EAX,CR0             ;Get new Control Register 0
  CMP  EBX,EAX             ;Did the Cache Status change?
  JE  >C70                 ;If it didn't change, it's incompatible
  CALL ToggleCache         ;If so, toggle it back to where it was
  STI                      ;Enable interrupts
  RET

C70:                       ;CPU is incompatible
  STI                      ;Enable interrupts
  MOV  DX,OFFSET CPUErrMsg ;Write the
  CALL WriteZErr           ;  Error Message
  MOV  BL,ErLvlError       ;ErrorLevel = CPU Error
  MOV  DX,OFFSET NoMsg     ;No message to write
  JMP  Exit                ;Quit

;------------------------------------------------------------------------------
;DISABLE THE CPU CACHE
;Inputs:
;Outputs:
;Changes: Disables the CPU Cache
;------------------------------------------------------------------------------
DisableCache:
  PUSH EAX                          ;Save used registers
  PUSHF                             ;Save flags
  CLI                               ;Disable interrupts
  MOV  EAX,CR0                      ;Disable
  OR   EAX,CacheDisable+NoWriteThru ;  the
  MOV  CR0,EAX                      ;  Cache
  NOP                               ;Waste some time
  MOV  EAX,CR0                      ;Get the Cache Status back again
  TEST EAX,CacheDisable             ;Is the Cache Disabled?
  JZ  >D90                          ;If not, we're done
  WBINVD                            ;Flush and Invalidate the Cache
  NOP                               ;Waste some time
D90:                                ;We're done
  POPF                              ;Restore flags
  POP  EAX                          ;Restore used registers
  RET

;------------------------------------------------------------------------------
;ENABLE THE CPU CACHE
;Inputs:
;Outputs:
;Changes: Enables the CPU Cache
;------------------------------------------------------------------------------
EnableCache:
  PUSH EAX                              ;Save used registers
  PUSHF                                 ;Save flags
  CLI                                   ;Disable interrupts
  MOV  EAX,CR0                          ;Enable
  AND  EAX,NOT CacheDisable+NoWriteThru ;  the
  MOV  CR0,EAX                          ;  Cache
  POPF                                  ;Restore flags
  POP  EAX                              ;Restore used registers
  RET

;------------------------------------------------------------------------------
;TOGGLE THE CPU CACHE
;Inputs:
;Outputs:
;Changes: Toggles the CPU Cache
;------------------------------------------------------------------------------
ToggleCache:
  PUSH EAX                          ;Save used registers
  PUSHF                             ;Save flags
  CLI                               ;Disable interrupts
  MOV  EAX,CR0                      ;Toggle
  XOR  EAX,CacheDisable+NoWriteThru ;  the
  MOV  CR0,EAX                      ;  Cache
  NOP                               ;Waste some time
  MOV  EAX,CR0                      ;Get the Cache Status back again
  TEST EAX,CacheDisable             ;Is the Cache Disabled?
  JZ  >T90                          ;If not, we're done
  WBINVD                            ;Flush and Invalidate the Cache
  NOP                               ;Waste some time
T90:                                ;We're done
  POPF                              ;Restore flags
  POP  EAX                          ;Restore used registers
  RET

;------------------------------------------------------------------------------
;TEST AND SEE IF THE CPU IS AN 80486+
;Inputs:
;Outputs: CF = Clear if the CPU is an 80486+
;            = Set if not an 80486+
;Changes:
;NOTE: This procedure uses 32-bit instructions!!
;------------------------------------------------------------------------------
Test80486:
  CALL Test80386   ;Is it at least an 80386?
  JC  >E95         ;If not, it can't be an 80486, either
  PUSH EAX,EBX     ;Save used registers
  PUSH CX          ;Save used registers
  MOV  CX,SP       ;Save the current stack pointer
  PUSHFD           ;Save extended flags
  AND  SP,0FFFCh   ;Align Stack Pointer to 32-bit line, to avoid Alignment Fault
  PUSHFD           ;Put current EFLAGS
  POP  EAX         ;  in EAX
  MOV  EBX,EAX     ;Save EAX in EBX
  XOR  EAX,4_0000h ;Try to change
  PUSH EAX         ;  the
  POPFD            ;  Alignment Flag
  PUSHFD           ;Put current EFLAGS
  POP  EAX         ;  in EAX
  CMP  EAX,EBX     ;Did the Alignment Flag change?
  JNE >E80         ;If so, it's an 80486
E70:               ;It's not an 80486
  POPFD            ;Restore extended flags
  STC              ;Set the not 80486 flag
  JMP >E90         ;We're done
E80:               ;It is an 80486
  POPFD            ;Restore extended flags
  CLC              ;Set the 80486 flag
E90:               ;We're done
  MOV  SP,CX       ;Restore the stack pointer
  POP  CX          ;Restore used registers
  POP  EBX,EAX     ;Restore used registers
  RET

E95:               ;Can't be 80486
  STC              ;Set the not 80486 flag
  RET

;------------------------------------------------------------------------------
;TEST AND SEE IF THE CPU IS AN 80386+ (CAN HANDLE 32-BIT INSTRUCTIONS)
;Inputs:
;Outputs: CF = Clear if the CPU is an 80386+
;            = Set if CPU is not an 80386+
;Changes:
;------------------------------------------------------------------------------
Test80386:
  PUSH AX,BX     ;Save used registers
  PUSHF          ;Save flags
  CALL Test8086  ;Is it an 8086?
  JNC >E70       ;If so, it can't be an 80386
  MOV  BX,0F000h ;Flags mask for testing (these bits always clear on 80286)
  PUSHF          ;Put the current flags
  POP  AX        ;  in AX
  OR   AX,BX     ;Try to set the
  PUSH AX        ;  four high bits
  POPF           ;  of the flags
  PUSHF          ;Put the current flags
  POP  AX        ;  in AX
  TEST AX,BX     ;Are any of the four high bits set?
  JNZ >E80       ;If so, it's not an 80286 - it must be an 80386+
E70:             ;It's not an 80386
  POPF           ;Restore flags
  STC            ;Set the not 80386 flag
  JMP >E90       ;We're done
E80:             ;It is an 80386
  POPF           ;Restore flags
  CLC            ;Set the 80386 flag
E90:             ;We're done
  POP  BX,AX     ;Restore used registers
  RET

;------------------------------------------------------------------------------
;TEST AND SEE IF THE CPU IS AN 8086/8088
;Inputs:
;Outputs: CF = Clear if the CPU is an 8086/8088
;            = Set if not at 8086/8088
;Changes:
;NOTE: Unlike the other CPU tests, this is not a "greater than or equal to".
;      The CPU must be EXACTLY an 8086/8088, or this returns with CF set.
;------------------------------------------------------------------------------
Test8086:
  PUSH AX,BX     ;Save used registers
  PUSHF          ;Save flags
  MOV  BX,0F000h ;Flags mask for testing (these bits always set on 8086/8088)
  PUSHF          ;Put the current flags
  POP  AX        ;  in AX
  AND  AX,0FFFh  ;Try to clear the
  PUSH AX        ;  four high bits
  POPF           ;  of the flags
  PUSHF          ;Put the current flags
  POP  AX        ;  in AX
  AND  AX,BX     ;Are the four high bits
  CMP  AX,BX     ;  of the flags set?
  JE  >E80       ;If so, it's an 8086/8088
  POPF           ;Restore flags
E70:             ;It is not an 8086/8088
  STC            ;Set the not 8086/8088 flag
  JMP >E90       ;We're done
E80:             ;It is an 8086/8088
  POPF           ;Restore flags
  CLC            ;Set the 8086/8088 flag
E90:             ;We're done
  POP  BX,AX     ;Restore used registers
  RET

;------------------------------------------------------------------------------
;CAPITALIZE A CHARACTER
;Inputs : AL = character to be capitalized
;Outputs: AL = capitalized character
;Changes:
;------------------------------------------------------------------------------
Capitalize:
  CMP  AL,'a'     ;Is it less than 'a'?
  JB  >C90        ;If so, quit
  CMP  AL,'z'     ;Is it more than 'z'?
  JA  >C90        ;If so, quit
  SUB  AL,'a'-'A' ;If between 'a' and 'z', capitalize it
C90:              ;We're done
  RET

;------------------------------------------------------------------------------
;WRITE ASCIIZ STRING TO CON OR TO ERR
;Inputs:  ES:[DX] = Pointer to string
;Outputs: Writes the string to CON or ERR
;Changes:
;------------------------------------------------------------------------------
WriteZCON:         ;Write to CON
  PUSH BX          ;Save used register
  MOV  BX,1        ;Device #1 (CON)
  JMP >Z10         ;Jump to do it
WriteZERR:         ;Write to ERR
  PUSH BX          ;Save used register
  MOV  BX,2        ;Device #2 (ERR)
Z10:               ;Write to CON or ERR
  PUSH AX,CX,DI    ;Save used registers
  MOV  DI,DX       ;Point DI at the string
  CALL CalcStrSize ;Calculate the size of the string (returns CX)
  JZ  >Z80         ;If nothing to write, just quit
  MOV  AH,040h     ;Function 40h (Write to Device)
  INT  021h        ;Do it
Z80:               ;We're done
  POP  DI,CX,AX    ;Restore used registers
  POP  BX          ;Restore used register
  RET

;------------------------------------------------------------------------------
;CALCULATE THE LENGTH OF AN ASCIIZ STRING
;Inputs : ES:[DI] = Pointer to string
;Outputs: CX = Length of the string
;         ZF = Set if length = 0
;            = Clear if length > 0
;Changes:
;------------------------------------------------------------------------------
CalcStrSize:
  PUSH AX,DI  ;Save used registers
  PUSH DI     ;Save the starting string pointer (we'll need it again)
  XOR  AL,AL  ;Look for a 0
  MOV  CX,-1  ;Start with max count for REPNE
  REPNE SCASB ;Find the end of the string (searches ES:[DI])
  MOV  CX,DI  ;Need the character count in CX
  POP  AX     ;Get the starting offset back again
  SUB  CX,AX  ;Calculate the number of characters
  DEC  CX     ;Exclude the final 0 (this also sets ZF for us)
  POP  DI,AX  ;Restore used registers
  RET